#ifndef COMMON_H
#define COMMON_H

#define FRIGO_PROTOCOL_VERSION 1
#define FRIGO_UDP_PORT 54639
#define FRIGO_TCP_PORT 54639
#define FRIGO_TCP_DATA_SIZE_KEY 42
#define FRIGO_MULTICAST_ADDRESS "225.42.42.42"
#define FRIGO_UUID_TTL 600000
#define FRIGO_TCP_RECONNECT_MIN 10000
#define FRIGO_TCP_RECONNECT_MAX 30000

#endif // COMMON_H

